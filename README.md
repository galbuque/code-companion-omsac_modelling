# Towards an Ontology-driven Approach to Model and Analyze Microservice Architectures - Source-code Companion

Companion of the paper "Towards an Ontology-driven Approach to Model and Analyze Microservice Architectures" submitted to the MEDES2021.
Authors: Gabriel Morais, Dominik Bork, Mehdi Adda
